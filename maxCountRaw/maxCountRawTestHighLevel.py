# N, M, K를 공백으로 구분하여 입력받기
n, m, k = map(int, input().split())
# N개의 수를 공백으로 구분하여 입력받기
data = list(map(int, input().split()))

data.sort()  # 입력받은 수 정렬
first = data[n-1]   # 가장 큰 수
second = data[n-2]  # 두 번째로 큰 수

# --- example ---
# N=5, M=8, K=3
# 2, 4, 5, 4, 6 (입력받은 N개의 자연수)

# 가장 큰 수가 더해지는 횟수 계산
count = int(m / (k+1)) * k  # 6
count += m % (k+1)  # 0 => 6+0 = 6

result = 0
result += count * first   # 가장 큰 수 더하기 6*6 = 36
result += (m-count) * second    # 두 번째로 큰 수 더하기  (8-6)*5 = 10  => result = 36+10 = 46

print(f'result : {result}')

