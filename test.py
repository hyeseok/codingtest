import time

n = 1260
count = 0

# 큰 단위의 화폐부터 차례대로 확인
lists = [500, 100, 50, 10]

start_time = time.time()  # 시작시간

for coin in lists:
    count += n / coin  # 해당 화폐로 거슬러 줄 수 있는 동전의 개수 세기
    n %= coin

end_time = time.time()  # 종료시간

print(f'count : {count}')
print(f'time : {end_time-start_time}')
