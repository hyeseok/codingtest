import time
# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

start_time = time.time()

array = [3, 5, 1, 2, 4]
summary = 0

for x in array:
    summary += x

end_time = time.time()
print(f'cal val = {summary} , cal_time : {end_time-start_time}')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
