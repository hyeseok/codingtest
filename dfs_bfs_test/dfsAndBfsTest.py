# 타겟 넘버 (DFS/BFS) 코딩테스트
def dfs(array, numbers, target, size):
    answer = 0
    print(f'array : {array}, len(array) : {len(array)} == size : {size}, numbers {numbers}')
    if len(array) == size:
        print(f'sum(array) : {sum(array)}')
        if sum(array) == target:
            return 1
        else:
            return 0
    else:
        A = numbers.pop(0)
        print(f'for start ---')
        for i in [1, -1]:
            print(f'i = {i}')
            array.append(A * i)
            answer += dfs(array, numbers, target, size)
            print(f'-----answer : {answer}-----')
            array.pop()
            print(f'array.pop() : {array}')
        print(f'for end ---')
        print(f'A : {A}')
        numbers.append(A)
        print(f'numbers.append(A) : {numbers}')
        return answer


def solution(numbers, target):
    answer = 0
    answer += dfs([numbers[0]], numbers[1:], target, len(numbers))
    print(f'** ----- sum answer : {answer} ------')
    answer += dfs([-numbers[0]], numbers[1:], target, len(numbers))
    print(f'** ----- sum answer : {answer} ------')
    return answer


print(f'return {solution([1, 1, 1, 1, 1], 3)}')
